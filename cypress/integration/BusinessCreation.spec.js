/// <reference types="Cypress" />

//const { contains } = require("cypress/types/jquery");

describe('Business Creation', function () {
    it ('Creating a new business', function() {
        cy.visit('http://orbitclientui.test.vggdev.com', { retryOnNetworkFailure: true });
        cy.reload()
        cy.clearCookies({log: true})
        cy.contains('Sign In').click()
        cy.get(':nth-child(1) > .withIcon > .form-control').type("pontius@yopmail.com")
        cy.get(':nth-child(2) > .withIcon > .form-control').type("P0ntius.")
        cy.get(".btn").click({force:true})
        cy.wait(12000)
        cy.get('.page-title')
        cy.contains('Overview')
        cy.wait(5000)
        cy.get('a.navigation-link').eq(8).click({force:true})
        cy.contains('Businesses')
        cy.get('.card-body > .btn').click()
        cy.get(':nth-child(1) > .ng-tns-c13-2 > .form-group > .withIcon > #name').type("Draculs")
        cy.get(':nth-child(2) > .ng-tns-c13-2 > .form-group > .withIcon > #name').type("Blood")
        cy.get(':nth-child(3) > .form-control').type("test desc")
        cy.get('.col-lg-12 > .btn').click()
        cy.get('.toast-error')
        cy.get('.toast-message')
        cy.contains('Business name already exist')
        cy.get('.close > .ng-tns-c13-2').click()
        cy.wait(2000)
        cy.get(':nth-child(3) > .nav-link > .fas').click()
        cy.get('.dropdown-menu > :nth-child(5) > span').click()

    }) 
})
