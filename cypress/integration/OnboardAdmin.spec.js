/// <reference types="Cypress"/>

describe ('Orbit Client', function () {
    it ('Onboarding a Client Admin', function () {
        cy.visit('http://orbitclientui.test.vggdev.com', { retryOnNetworkFailure: true });
        cy.reload()
        cy.clearCookies({log: true})
        cy.contains('Sign Up').click()
        cy.get('strong')
        cy.wait(3000)
        cy.get(':nth-child(1) > app-input > .form-group > .withIcon > #name').type("kenee@yopmail.com")
        cy.get(':nth-child(2) > app-input > .form-group > .withIcon > #name').type("Lene")
        cy.get(':nth-child(3) > app-input > .form-group > .withIcon > #name').type("Qene")
        cy.get(':nth-child(4) > app-input > .form-group > .withIcon > #name').type("EMMNEE")
        cy.get(':nth-child(5) > .withIcon > .form-control').type("P@ssw0rd")
        cy.get(':nth-child(6) > .withIcon > .form-control').type("P@ssw0rd")
        cy.get('.btn').click({force:true})
        cy.wait(5000)
        cy.get('.toast-error')
        cy.contains('kenee@yopmail.com already exist')       
        
    })
})   