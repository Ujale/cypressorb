/// <reference types="Cypress"/>

const { should } = require("chai");

describe ('My First Test', function () {
    it ('Login to Orbit test web', function() {
        cy.visit('http://orbitclientui.test.vggdev.com', { retryOnNetworkFailure: true });
        cy.contains('Sign In').click()
        cy.get(':nth-child(1) > .withIcon > .form-control').type("pontius@yopmail.com")
        cy.get(':nth-child(2) > .withIcon > .form-control').type("P0ntius.")
        cy.get(".btn").click({force:true})
        cy.wait(12000)
        
    })
    it('Should contain correct url', () => {
        cy.url().should('contain', 'http://orbitclientui.test.vggdev.com')
    })
    it('Should display Dasboard', () => {
        cy.get(':nth-child(3) > .nav-link').should('be.visible')
    })
})