/// <reference types="cypress"/>

describe('Business Account Creation', function () {
    it ('Login to Orbit test web', function() {
        cy.visit('http://orbitclientui.test.vggdev.com', { retryOnNetworkFailure: true });
        cy.reload()
        cy.clearCookies({log: true})
        cy.contains('Sign In').click()
        cy.get(':nth-child(1) > .withIcon > .form-control').type("pontius@yopmail.com")
        cy.get(':nth-child(2) > .withIcon > .form-control').type("P0ntius.")
        cy.get(".btn").click({force:true})
        cy.wait(12000)
        // cy.get('.toast-success')
        // cy.get('.toast-title')
        // cy.contains('Welcome')
        // cy.get('.toast-message')
        // cy.contains('pontius@yopmail.com')
        cy.get('.page-title')
        cy.contains('Overview')
        cy.contains('Settings')
        cy.wait(4000)
        cy.get('a.navigation-link').eq(9).click({force:true})
        cy.get('.card-body > .btn').click()
        cy.get(':nth-child(1) > .input-container > .form-control').select('Draculs')
        cy.get('.ng-tns-c13-2.ng-invalid > .row > :nth-child(2) > .input-container > .form-control').select('GTBANK')
        cy.get('.col-lg-5 > .form-control').type("0014109992")
        cy.wait(5000)
        cy.get('.col-lg-7 > .input-container > .form-control').click
        cy.wait(4000)
        cy.get('.ng-tns-c13-2.ng-dirty > .btn').click({force:true})
        cy.wait(18000)
        cy.get('.ng-tns-c13-2.ng-dirty > .btn').click({force:true})
        
    })
})