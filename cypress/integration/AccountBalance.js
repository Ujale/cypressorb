/// <reference types="cypress"/>

describe('Account Balance', function () {
    it ('Login to Orbit test web', function() {
        cy.visit('http://orbitclientui.test.vggdev.com', { retryOnNetworkFailure: true });
        cy.contains('Sign In').click()
        cy.get(':nth-child(1) > .withIcon > .form-control').type("crusio@yopmail.com")
        cy.get(':nth-child(2) > .withIcon > .form-control').type("P@ssw0rd1")
        cy.get(".btn").click({force:true})
        cy.wait(12000)
        cy.get('.toast-success')
        cy.get('.toast-title')
        cy.contains('Welcome')
        cy.get('.toast-message')
        cy.contains('crusio@yopmail.com')
        cy.get('.page-title')
        cy.contains('Overview')
        cy.contains('Settings')
        cy.wait(4000)
        cy.get('a.navigation-link').eq(9).click({force:true})
        cy.get(':nth-child(2) > .sorting_1 > .btn > .fas').click()
        cy.wait(10000)
        cy.get('.close > .ng-tns-c13-2').click()

    })
})