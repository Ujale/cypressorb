/// <reference types="Cypress"/>

describe ('Create a client user', function () {
    it ('Creating a new business', function() {
        cy.visit('http://orbitclientui.test.vggdev.com', { retryOnNetworkFailure: true });
        cy.reload()
        cy.clearCookies({log: true})
        cy.get(':nth-child(1) > .withIcon > .form-control').type("pontius@yopmail.com")
        cy.get(':nth-child(2) > .withIcon > .form-control').type("P0ntius.")
        cy.get(".btn").click({force:true})
        cy.wait(12000)
        cy.get('body')
        cy.contains('Settings')
        cy.wait(4000)
        cy.get('a.navigation-link').eq(10).click({force:true})
        cy.contains('Users')
        cy.get('.card-body > .btn').click()
        cy.get(':nth-child(1) > .form-control').type("Frand")
        cy.get(':nth-child(2) > .form-control').type("New")
        cy.get(':nth-child(3) > .form-control').type("frand@yopmail.com")
        cy.get(':nth-child(4) > .form-control').type("P@ssw0rd")
        cy.get('#confirmPassword').type("P@ssw0rd")
        cy.get('div.ng-tns-c13-2 > .btn').click({force:true})
        cy.get('.toast-message')
        


    }) 
})